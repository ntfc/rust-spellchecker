use bit_vec::BitVec;
use std::io::Cursor;
use murmur3::murmur3_x64_128;

pub struct BloomFilter {
    size: usize,
    bit_set: BitVec,
}

impl BloomFilter {
    pub fn new(size: usize) -> Self {
        Self {
            size,
            bit_set: BitVec::from_elem(size, false)
        }
    }

    pub fn add(&mut self, word: &str) {
        self.bit_set.set(self.hash_word(word), true);
    }

    pub fn contains_word(&self, word: &str) -> bool {
        let word_hash = self.hash_word(word);
        self.bit_set.get(word_hash).unwrap()
    }

    fn hash_word(&self, word: &str) -> usize {
        let hash: u128 = murmur3_x64_128(&mut Cursor::new(word), 0).unwrap();
        (hash % (self.size as u128)) as usize
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_creation() {
        let bloom_filter = BloomFilter::new(10);
        assert_eq!(true, bloom_filter.bit_set.none());
    }

    #[test]
    fn test_hash_word() {
        let word = "nuno";
        let mut bloom_filter = BloomFilter::new(10);

        assert_eq!(true, bloom_filter.bit_set.none());
        bloom_filter.add(&word);
        assert_eq!(false, bloom_filter.bit_set.none());
    }

    #[test]
    fn test_hash_and_find_word() {
        let word = "nuno";
        let same_word = "nuno";
        let mut bloom_filter = BloomFilter::new(10);

        bloom_filter.add(&word);

        assert_eq!(true, bloom_filter.contains_word(&word));
        assert_eq!(true, bloom_filter.contains_word(&same_word));
        assert_eq!(false, bloom_filter.contains_word("does not exist"));
    }
}
