use argh::FromArgs;

#[derive(FromArgs)]
/// Simple spellchecker using a Bloom Filter
pub struct Args {
    /// number of bits used for the bloom filter
    #[argh(option, default = "1024")]
    pub nbits: usize,

    /// dictionary file
    #[argh(option)]
    pub dict_file: std::path::PathBuf, // PathBuf has the FromStr trait implementation
}

